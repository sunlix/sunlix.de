/**
 * Used modules
 */

var gulp       = require('gulp'),
    plugins    = require('gulp-load-plugins')(),
    del        = require('del'),
    vinylPaths = require('vinyl-paths'),
    merge      = require('merge-stream');

/**
 * Path variables
 */

var base                  = '.',
    src                   = base + '/src',
    assets                = base + '/assets',
    assets_js             = assets + '/js',
    assets_js_libs        = assets_js + '/libs',
    assets_css            = assets + '/css',
    assets_img            = assets + '/img',
    assets_fonts          = assets + '/fonts',
    fileset = {
        base: [
            '!' + src + '/**',
            '!' + base + '/node_modules/**',
            '!' + base + '/bower_components/**',
            '!gulpfile.js',
            '**/*.html',
            '**/*.css',
            '**/*.js',
        ],
        img: [
            '**/*.gif',
            '**/*.jpg',
            '**/*.png',
        ],
        fonts: [
            '**/*.eot',
            '**/*.otf',
            '**/*.svg',
            '**/*.ttf',
            '**/*.woff',
            '**/*.woff2',
        ],
        toArray: function() {
            return fileset.base.concat(fileset.img.concat(fileset.fonts))
        }
    };

gulp.task('default', function() {
    console.log('Execute "gulp compile|compile:production" to only compile the source files');
});

/**
 * Assets tasks
 */

gulp.task('assets:find', ['clean'], function() {
    var j = gulp
                .src([
                    '**/*.js',
                    '!**/jquery.*.js'
                ], {cwd: src})
                .pipe(plugins.flatten())
                .pipe(gulp.dest(assets_js));

    var l = gulp
                .src('**/jquery.*.js', {cwd: src})
                .pipe(plugins.flatten())
                .pipe(gulp.dest(assets_js_libs));

    var i = gulp
                .src(fileset.img, {cwd: src})
                .pipe(plugins.flatten())
                .pipe(gulp.dest(assets_img));

    var f = gulp
                .src(fileset.fonts, {cwd: src})
                .pipe(plugins.flatten())
                .pipe(gulp.dest(assets_fonts));

    return merge(j, l, i, f);
});

gulp.task('assets:concat', ['assets:find'], function() {
    return gulp
            .src('*', {cwd: assets_js_libs})
            .pipe(plugins.concat('jquery.libs.js'))
            .pipe(gulp.dest(assets_js));
});

/**
 * Compile tasks
 */

gulp.task('watch', function () {
    plugins.livereload.listen();
    return gulp
            .watch(['**/*.scss', '**/*.js'], {cwd: src}, ['compile']);
});

gulp.task('compile', ['assets:concat'], function () {
    return gulp
            .src(src + '/**/*.scss')
            .pipe(plugins.sourcemaps.init())
            .pipe(plugins.sass().on('error', plugins.sass.logError))
            .pipe(plugins.cssnano({
                discardComments: {
                    removeAll: true
                },
                autoprefixer: {
                    browsers: ['last 2 versions'],
                    cascade: false,
                    add: true
                }
            }))
            .pipe(plugins.sourcemaps.write('.'))
            .pipe(gulp.dest(assets_css))
            .pipe(plugins.livereload());
});

gulp.task('compile:production', ['compile'], function() {
    return gulp
            .src([
                '**/*.js',
                '!/**/*.min.js'
            ], {cwd: assets_js})
            .pipe(plugins.uglify())
            .pipe(plugins.rename({
                extname: '.min.js'
            }))
            .pipe(gulp.dest(assets_js));
});

/**
 * Clean tasks
 */

gulp.task('clean', function() {
    return gulp
            .src([
                assets,
                base + '/.sass-cache'
            ])
            .pipe(vinylPaths(del));
});
